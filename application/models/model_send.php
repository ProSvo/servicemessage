<?php

class Model_Send extends Model
{
	
	public function get_data()
	{
		try {
				$data = parent::read();
			} catch(PDOException $e) {
				echo 'Error: ' . $e->getMessage();	
			}
		return $data;
	}
	
	public function get_send()
	{
		try {
			  	$user_id = $_SESSION['user_id'];
			  	$user_sender = $_SESSION['user_sender'];
				$data = parent::read($user_id, $user_sender);
			} catch(PDOException $e) {
			  	echo 'Error: ' . $e->getMessage();	
			}
		return $data;
	}
	
		public function get_receive()
	{
		try {
			  	$user_id = $_SESSION['user_id'];
				$user_sender = $_SESSION['user_sender'];
				$data = parent::read($user_sender, $user_id);
			} catch(PDOException $e) {
			  	echo 'Error: ' . $e->getMessage();	
			}
		return $data;
	}
	
	public function set_data()
	{
		try {
			  	$description = $_POST['text'];
			  	$user_id = $_POST['user_id'];
			  	$user_sender = $_POST['user_sender'];
				parent::create_message($description, $user_id, $user_sender);		  
			} catch(PDOException $e) {
		 		echo 'Error: ' . $e->getMessage();	
			}
	}
}