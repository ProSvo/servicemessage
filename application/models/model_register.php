<?php

class Model_Register extends Model
{
	public function get_data()
	{
		try {
				$login = $_POST['login'];
				$data = parent::read($login);
			} catch(PDOException $e) {
				echo 'Error: ' . $e->getMessage();	
			}
		return $data;
	}

	public function set_data()
	{
		try {
			  	$login = $_POST['login'];
			  	$password = $_POST['password'];
			  	$email = $_POST['email'];
			  	parent::create_user($login, $password, $email);
			} catch(PDOException $e) {
			  	echo 'Error: ' . $e->getMessage();	
			}
	}
}