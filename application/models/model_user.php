<?php

class Model_User extends Model
{
	
	public function get_data()
	{
		try {
				$data = parent::read();
			} catch(PDOException $e) {
				echo 'Error: ' . $e->getMessage();	
			}
		return $data;
	}

	public function set_data()
	{
		try {
				session_start();
				$login = $_SESSION['login'];
				$date_last = date('Y-m-d H:i:s');
				parent::update($date_last, $login);
			} catch(PDOException $e) {
				echo 'Error: ' . $e->getMessage();	
			}

	}
}