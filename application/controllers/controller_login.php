<?php

class Controller_Login extends Controller
{
	function __construct()
	{
		$this->model = new Model_Login();
		$this->view = new View();
	}
		
	function action_index()
	{
		if(isset($_POST['login']) && isset($_POST['password']))
		{
			$login = $_POST['login'];
			$password = $_POST['password'];

			if (empty($login) or empty($password)) 
			{
				$data["login_status"] = "access_denied";
			} 
			else 
			{
				$databd = $this->model->get_data();
				foreach($databd as $key)
				{
				   $loginbd = $key['login'];
				   $passwordbd = $key['password'];
				}
				
				if($login == $loginbd && $password == $passwordbd)
				{
					$data["login_status"] = "access_granted";
	
					if($login == 'admin')
					{
						header('Location:/admin');
					}
					else 
					{
						session_start();
						$_SESSION['login'] = $login; 
						header('Location:/user');
					}
				}
				else 
				{
					$data["login_status"] = "access_denied";
				}
			}
		}
		else
		{
			$data["login_status"] = "";
		}
		
		$this->view->generate('login_view.php', 'template_view.php', $data);
	}
}