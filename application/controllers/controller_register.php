<?php

class Controller_Register extends Controller
{
	function __construct()
	{
		$this->model = new Model_Register();
		$this->view = new View();
	}
	
	function action_index()
	{
		$data["login_status"] = "";
		
		if(isset($_POST['login']) && isset($_POST['email']) && isset($_POST['password']) && isset($_POST['password_confirm']) )
		{
			$login = $_POST['login'];
			$email = $_POST['email'];
			$password = $_POST['password'];
			$password_confirm = $_POST['password_confirm'];
			$date_reg = date('Y-m-d');
			
			if (empty($login)  or empty($email) or empty($password) or empty($password_confirm)) 
			{
				$data["login_status"] = "field_empty";
			} 
			else 
			{
				$databd = $this->model->get_data();

				foreach($databd as $key)
				{
				   $loginbd = $key['login'];
				}
				
				if(!empty($loginbd))
				{
					$data["login_status"] = "login_repeat";
				}
				
				if($password != $password_confirm)
				{
					$data["login_status"] = "pass_repeat";
				}

				$pattern = "/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/";
		        if(!preg_match($pattern, $email)) 
		        {
		            $data["login_status"] = "email_incorrect";
		        }
				
				if($data["login_status"] == "")
				{
					$this->model->set_data();
					header('Location:/profile/');
				}
			}
		}
		else
		{
			$data["login_status"] = "";
		}
		
		$this->view->generate('register_view.php', 'template_view.php', $data);
	}

}