<?php

class Controller_User extends Controller
{
	function __construct()
	{
		$this->model = new Model_User();
		$this->view = new View();
	}
	
	function action_index()
	{
		$data = $this->model->get_data();
		$this->view->generate('user_view.php', 'template_view.php', $data);
	}
	
	function action_logout()
	{
		$this->model->set_data();
		header('Location:/login');
	}

	function action_send()
	{
		$this->view->generate('send_view.php', 'template_view.php');
	}
}