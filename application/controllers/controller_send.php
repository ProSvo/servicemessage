<?php

class Controller_Send extends Controller
{
	function __construct()
	{
		$this->model = new Model_Send();
		$this->view = new View();
	}
	
	function action_index()
	{
		session_start();
		if (isset($_POST['text']) && isset($_POST['user_id']) && isset($_POST['user_sender']))	
		{
			$description = $_POST['text'];
			$user_id = $_POST['user_id'];
			$user_sender = $_POST['user_sender'];
	
			
			$data_bd = $this->model->get_data();
	
			foreach($data_bd as $key)
			{
			   if ($key['login'] == $user_id) $_POST['user_id'] = $key['id'];
			   if ($key['login'] == $user_sender) $_POST['user_sender'] = $key['id'];
			}
	
			$data = $this->model->set_data();
			header('Location:/user');
		}
		$user_id = $_SESSION['login'];
		$user_sender = $_COOKIE['user'];
		
		$data_bd = $this->model->get_data();
		foreach($data_bd as $key)
		{
		   if ($key['login'] == $user_id) $_SESSION['user_id'] = $key['id'];
		   if ($key['login'] == $user_sender) $_SESSION['user_sender'] = $key['id'];
		}
		
		$data_send = $this->model->get_send();
		$data_receive = $this->model->get_receive();
		
		$data_merge = array_merge($data_send,$data_receive);
		asort($data_merge);
		$arr = array_reverse($data_merge);
		
		$this->view->generate('send_view.php', 'template_view.php', $arr);
	}

}