<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<title>SystemMessage</title>
		<link rel="stylesheet" type="text/css" href="/css/style.css" />
		<script type="text/javascript">
			function user(elem) {
				 document.cookie="user="+elem;
			};
		</script>
	</head>
	
	<body>
		<div id="wrapper">
			<div id="header">
				<div id="logo">
					<a href="/">System Message</a>
				</div>
				<div id="menu">
					<ul>
						<li class="first active"><a href="/">Головна</a></li>
						<li><a href="/login">Вхід</a></li>
						<li><a href="/register">Реєстрація</a></li>
						<li class="last"><a href="/contacts">Контакти</a></li>
					</ul>
					<br class="clearfix" />
				</div>
			</div>
			<div id="page">
				<div id="sidebar">
					<div>
						<h3>Основне меню</h3>
						<ul class="list">
							<li class="first "><a href="/">Головна</a></li>
							<li><a href="/login">Вхід</a></li>
							<li><a href="/register">Реєстрація</a></li>
							<li class="last"><a href="/contacts">Контакти</a></li>
						</ul>
					</div>
				</div>
				<div id="content">
					<div class="box">
						<?php include 'application/views/'.$content_view; ?>
					</div>
					<br class="clearfix" />
				</div>
				<br class="clearfix" />
			</div>
		</div>
		<div id="footer">
			<a href="/">SMS</a> &copy; 2012</a>
		</div>
	</body>
</html>
