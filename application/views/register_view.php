<h2>Реєстрація</h2>
<?php extract($data);?>
<p>
<form action="" method="post">
	<p>
    	<label>Логін:<br></label>
   		<input name="login" type="text" size="20" maxlength="20" value=<?php if(isset($_POST['login'])) echo $_POST['login'];?>>
   		 <?php if($login_status=="login_repeat") { ?>
			<span style="color:red">Користувач з таким іменем вже існує</span>
		<?php } ?>
    </p>
   
    <p>
    	<label>Електронна пошта:<br></label>
   		<input name="email" type="text" placeholder="user@example.com" value=<?php if(isset($_POST['email'])) echo $_POST['email'];?>>
   		 <?php if($login_status=="email_incorrect") { ?>
			<span style="color:red">Невірна поштова адреса</span>
		<?php } ?>
    </p>
	<p>
    	<label>Пароль:<br></label>
    	<input name="password" type="password" placeholder="6-8 символів" size="20" maxlength="20">
    </p>
	<p>
    	<label>Повторити пароль:<br></label>
    	<input name="password_confirm" placeholder="Повторіть пароль" type="password" size="20" maxlength="20">
   		 <?php if($login_status=="pass_repeat") { ?>
			<span style="color:red">Паролі не співпадають</span>
		<?php } ?>

    </p>
	<p>
    	<input type="submit" name="submit" value="Реєстрація">
	</p>
</form>
</p	>
 <?php if($login_status=="field_empty") { ?>
	<span style="color:red">Заповніть усі поля</span>
<?php } ?>
