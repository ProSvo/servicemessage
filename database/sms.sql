-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Час створення: Гру 12 2012 р., 00:05
-- Версія сервера: 5.5.28
-- Версія PHP: 5.3.10-1ubuntu3.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- БД: `sms`
--

-- --------------------------------------------------------

--
-- Структура таблиці `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_sender` int(11) NOT NULL,
  `date_send` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=47 ;

--
-- Дамп даних таблиці `messages`
--

INSERT INTO `messages` (`id`, `description`, `user_id`, `user_sender`, `date_send`) VALUES
(19, 'Hello', 12, 37, '2012-12-10 12:11:24'),
(20, 'How are you?', 12, 43, '2012-12-10 12:14:07'),
(21, 'Hi', 12, 44, '2012-12-10 12:14:25'),
(22, 'how do you do?', 12, 22, '2012-12-10 12:14:44'),
(23, 'I''m fine', 12, 22, '2012-12-10 12:14:58'),
(24, 'message', 12, 37, '2012-12-10 12:15:02'),
(25, 'wert', 12, 22, '2012-12-10 12:27:17'),
(26, 'ok good luck', 12, 22, '2012-12-10 12:30:36'),
(28, 'who are you?', 13, 12, '2012-12-10 13:05:35'),
(29, 'message 2', 13, 43, '2012-12-10 13:05:38'),
(30, 'message 3', 13, 12, '2012-12-10 13:05:44'),
(31, 'letter', 13, 12, '2012-12-10 13:05:48'),
(32, 'message 4', 13, 12, '2012-12-10 13:05:58'),
(35, 'letter new', 12, 13, '2012-12-10 15:19:47'),
(37, 'Are you free?', 13, 12, '2012-12-10 15:30:36'),
(38, 'hello', 13, 12, '2012-12-10 15:30:44'),
(40, 'OK', 13, 12, '2012-12-10 15:31:55'),
(41, 'hello dfsdf', 12, 43, '2012-12-10 16:51:44'),
(42, 'good day', 43, 12, '2012-12-10 16:52:18'),
(43, 'no', 43, 12, '2012-12-10 16:52:23'),
(44, 'message 2d', 12, 43, '2012-12-10 16:52:43'),
(45, 'cool', 43, 12, '2012-12-10 16:53:13');

-- --------------------------------------------------------

--
-- Структура таблиці `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `email` varchar(25) NOT NULL,
  `date_reg` date NOT NULL,
  `date_last` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=47 ;

--
-- Дамп даних таблиці `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `email`, `date_reg`, `date_last`) VALUES
(11, 'Roman', 'qwerty', 'roman@gmail.com', '2012-12-08', '2012-12-10 17:09:15'),
(12, 'ProSvo', 'qwerty', 'prosvo@gmail.com', '2012-12-07', '2012-12-12 00:03:49'),
(13, 'Neo', 'qwerty', 'neo@yahoo.com', '2012-12-08', '2012-12-10 18:28:54'),
(22, 'admin', 'qwerty', 'admin@gmail.com', '2012-12-07', '2012-12-09 17:39:24'),
(34, 'Petro', 'qwerty', 'petro@mail.com', '2012-12-09', '2012-12-10 19:14:13'),
(37, 'Vasya', 'qwerty', 'usere@ex.com', '2012-12-09', '2012-12-10 13:54:00'),
(43, 'Olya', 'qwerty', 'olya@mail.com', '2012-12-09', '2012-12-10 16:53:22'),
(44, 'Gal', 'qwerty', 'user@examle.com', '2012-12-09', '2012-12-09 15:40:33'),
(45, 'ret', 'qwerty', 're@ex.ia', '2012-12-10', '2012-12-11 19:43:29'),
(46, 'Olko', 'qwerty', 'olko@mail.com', '2012-12-10', '2012-12-10 13:21:42');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
